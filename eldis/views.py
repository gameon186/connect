import requests, xlwt
from .models import ListTU, ReadingTSDay, ReadingGVSDay, ReadingGVSHour, ReadingTSHour, CheckParams, Tags, ObjectsEldis
from django.views.generic import ListView, DetailView, UpdateView, TemplateView
from datetime import datetime, timedelta
from django.core.paginator import Paginator
from django.db.models import Q
from django.http import HttpResponse
from django.db.models import Avg, Sum
from django.contrib.auth.views import LoginView, LogoutView
from .forms import AuthUserForm, CheckForm
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User


# Функция экспорта списка точек учета в Excel
def export_list(request):
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="list.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Список точек учета')

    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    columns = ['Потребитель', 'Наименование объекта', 'Адрес объекта', 'Модель ПУ', 'Номер ПУ', 'Точка учета', 'Номер ТУ', 'Ресурс']

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    font_style = xlwt.XFStyle()

    search = request.GET.get('q', '')
    rows = ListTU.objects.filter(Q(idObject__tags__name__icontains=search) | Q(nameObject__icontains=search) | Q(addressObject__icontains=search) | Q(modelPU__icontains=search) | Q(numberPU__icontains=search) | Q(point__icontains=search) | Q(resource__icontains=search)).values_list('idObject__consumer', 'nameObject', 'addressObject', 'modelPU', 'numberPU', 'point', 'idPoint', 'resource')
    for row in rows:
        row_num += 1
        for col_num in range(len(row)):
            ws.write(row_num, col_num, row[col_num], font_style)

    wb.save(response)
    return response


# Функция экспорта показаний ГВС в Excel
def export_gvs(request):
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="gvs.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Показания ГВС')

    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    columns = ['Дата', 'Q1', 'Q1 Итог', 'Q2', 'Q2 Итог', 't1', 't2', 'V1', 'V1 Итог', 'V2', 'V2 Итог', 'M1', 'M1 Итог',
               'M2', 'M2 Итог', 'P1', 'P2', 'Q', 'Q Итог', 'dt', 'dV', 'dM', 'tхв', 'Pхв', 'TGmax', 'TGmin', 'Tdt',
               'TFault', 'Toff', 'T прочие НС', 'Тр', 'Тр Итог', 'Тн', 'Тн Итог', 'НС']

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    font_style = xlwt.XFStyle()
    date_style = xlwt.XFStyle()
    date_style.num_format_str = 'dd.mm.yyyy HH:MM'

    id = request.GET.get('id')
    startDate = request.GET.get('startDate')
    endDate = request.GET.get('endDate')
    typeArchive = request.GET.get('typeArchive')

    if (typeArchive == '30004'):
        rows = ReadingGVSDay.objects.filter(idPoint=id, date__range=(startDate, endDate)).values_list('date', 'Q1', 'Q1_Total', 'Q2', 'Q2_Total', 't1', 't2', 'V1', 'V1_Total', 'V2', 'V2_Total', 'M1', 'M1_Total', 'M2', 'M2_Total', 'P1', 'P2', 'Q', 'Q_Total', 'dt', 'dV', 'dM', 'tcw', 'Pcw', 'TGmax', 'TGmin', 'Tdt', 'TFault', 'Toff', 'TOtherNS', 'QntHIP', 'QntHIP_Total', 'QntP', 'QntP_Total', 'ns')
    else:
        rows = ReadingGVSHour.objects.filter(idPoint=id, date__range=(startDate, endDate)).values_list('date', 'Q1', 'Q1_Total', 'Q2', 'Q2_Total', 't1', 't2', 'V1', 'V1_Total', 'V2', 'V2_Total', 'M1', 'M1_Total', 'M2', 'M2_Total', 'P1', 'P2', 'Q', 'Q_Total', 'dt', 'dV', 'dM', 'tcw', 'Pcw', 'TGmax', 'TGmin', 'Tdt', 'TFault', 'Toff', 'TOtherNS', 'QntHIP', 'QntHIP_Total', 'QntP', 'QntP_Total', 'ns')
    for row in rows:
        row_num += 1
        for col_num in range(len(row)):
            if (col_num == 0):
                ws.write(row_num, col_num, row[col_num], date_style)
            else:
                ws.write(row_num, col_num, row[col_num], font_style)

    wb.save(response)
    return response


# Функция экспорта показаний ТС в Excel
def export_ts(request):
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="ts.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Показания ТС')

    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    columns = ['Дата', 'Q1', 'Q1 Итог', 'Q2', 'Q2 Итог', 't1', 't2', 't3', 'V1', 'V1 Итог', 'V2', 'V2 Итог', 'V3', 'V3 Итог', 'M1', 'M1 Итог',
               'M2', 'M2 Итог', 'M3', 'M3 Итог', 'P1', 'P2', 'P3', 'Q', 'Q Итог', 'dt', 'dV', 'dM', 'ta', 'tхв', 'Pхв', 'TGmax', 'TGmin', 'Tdt',
               'TFault', 'Toff', 'T прочие НС', 'Тр', 'Тр Итог', 'Тн', 'Тн Итог', 'НС']

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    font_style = xlwt.XFStyle()
    date_style = xlwt.XFStyle()
    date_style.num_format_str = 'dd.mm.yyyy HH:MM'

    id = request.GET.get('id')
    startDate = request.GET.get('startDate')
    endDate = request.GET.get('endDate')
    typeArchive = request.GET.get('typeArchive')

    if (typeArchive == '30004'):
        rows = ReadingTSDay.objects.filter(idPoint=id, date__range=(startDate, endDate)).values_list('date', 'Q1', 'Q1_Total', 'Q2', 'Q2_Total', 't1', 't2', 't3', 'V1', 'V1_Total', 'V2', 'V2_Total', 'V3', 'V3_Total', 'M1', 'M1_Total', 'M2', 'M2_Total', 'M3', 'M3_Total', 'P1', 'P2', 'P3', 'Q', 'Q_Total', 'dt', 'dV', 'dM', 'ta', 'tcw', 'Pcw', 'TGmax', 'TGmin', 'Tdt', 'TFault', 'Toff', 'TOtherNS', 'QntHIP', 'QntHIP_Total', 'QntP', 'QntP_Total', 'ns')
    else:
        rows = ReadingTSHour.objects.filter(idPoint=id, date__range=(startDate, endDate)).values_list('date', 'Q1', 'Q1_Total', 'Q2', 'Q2_Total', 't1', 't2', 't3', 'V1', 'V1_Total', 'V2', 'V2_Total', 'V3', 'V3_Total', 'M1', 'M1_Total', 'M2', 'M2_Total', 'M3', 'M3_Total', 'P1', 'P2', 'P3', 'Q', 'Q_Total', 'dt', 'dV', 'dM', 'ta', 'tcw', 'Pcw', 'TGmax', 'TGmin', 'Tdt', 'TFault', 'Toff', 'TOtherNS', 'QntHIP', 'QntHIP_Total', 'QntP', 'QntP_Total', 'ns')
    for row in rows:
        row_num += 1
        for col_num in range(len(row)):
            if (col_num == 0):
                ws.write(row_num, col_num, row[col_num], date_style)
            else:
                ws.write(row_num, col_num, row[col_num], font_style)

    wb.save(response)
    return response


# Класс для отображения начальной страницы со списком точек учета
class DoubleListPUView(LoginRequiredMixin, ListView):
    login_url = reverse_lazy('login_page')
    redirect_field_name = ''
    model = ListTU
    template_name = 'eldis/double_list.html'
    context_object_name = 'listPU'

    def get_queryset(self):
        order = self.request.GET.get('list_sort', 'nameObject')
        search = self.request.GET.get('q', '')
        # Проверка - если поисковое поле пустое, то выводить весь список, иначе по фильтрации
        if (search == ''):
            context = ListTU.objects.all().order_by(order)
        else:
            context = ListTU.objects.filter(Q(idObject__tags__name__icontains=search) | Q(nameObject__icontains=search) | Q(addressObject__icontains=search) | Q(modelPU__icontains=search) | Q(numberPU__icontains=search) | Q(point__icontains=search) | Q(resource__icontains=search)).order_by(order).distinct()
        return context

    def get_context_data(self, **kwargs):
        context = super(DoubleListPUView, self).get_context_data(**kwargs)
        context['order_list'] = self.request.GET.get('list_sort', 'nameObject')
        context['q'] = self.request.GET.get('q', '')
        return context


# Класс для отображения показаний
class DoubleDetailView(LoginRequiredMixin, DetailView):
    login_url = reverse_lazy('login_page')
    redirect_field_name = ''
    model = ListTU
    context_object_name = 'getPU'
    template_name = 'eldis/double_detail.html'
    def get_context_data(self, **kwargs):
        startDate = self.request.GET.get('startDate', '2020-06-01T00:00')
        endDate = self.request.GET.get('endDate', '2020-08-01T00:00')
        starttime = datetime.strptime(startDate, '%Y-%m-%dT%H:%M')
        endtime = datetime.strptime(endDate, '%Y-%m-%dT%H:%M')
        typeArchive = self.request.GET.get('typeArchive', '30003')
        options = self.request.GET.get('options')
        order_list = self.request.GET.get('list_sort', 'nameObject')
        order = self.request.GET.get('sort', 'date')
        check = self.request.GET.get('check')
        month = self.request.GET.get('month', '2020-08')
        search = self.request.GET.get('q', '')
        # Если выбран фильтр по месяцам, то выставляем начальный и конечный период выбранного месяца
        if (check == 'on'):
            monthC = month + '-01 00:00:00'
            monthCh = datetime.strptime(monthC, '%Y-%m-%d %H:%M:%S')
            monthStart = monthCh.month
            monthNext = monthStart + 1
            if (monthNext == 13):
                monthNext = 1
            yearStart = monthCh.year
            if (monthNext == 1):
                yearNext = yearStart + 1
            else:
                yearNext = yearStart
            starttime = datetime(yearStart, monthStart, 1)
            endtime = datetime(yearNext, monthNext, 1)
            startDate = datetime.strftime(starttime, '%Y-%m-%dT%H:%M')
            endDate = datetime.strftime(endtime, '%Y-%m-%dT%H:%M')
        context = super(DoubleDetailView, self).get_context_data(**kwargs)
        # Если тип архива суточный, то загружаем суточные данные, также проверяем фильтры - если пустые - выдаем все данные, иначе выбираем в указанном промежутке
        if (typeArchive == "30004"):
            if (startDate == '' or endDate == ''):
                gvs = ReadingGVSDay.objects.filter(idPoint=self.get_object()).order_by(order)
                ts = ReadingTSDay.objects.filter(idPoint=self.get_object()).order_by(order)
            else:
                gvs = ReadingGVSDay.objects.filter(idPoint=self.get_object(), date__range=(starttime, endtime)).order_by(order)
                ts = ReadingTSDay.objects.filter(idPoint=self.get_object(), date__range=(starttime, endtime)).order_by(order)
        else:
            if (startDate == '' or endDate == ''):
                gvs = ReadingGVSHour.objects.filter(idPoint=self.get_object()).order_by(order)
                ts = ReadingTSHour.objects.filter(idPoint=self.get_object()).order_by(order)
            else:
                gvs = ReadingGVSHour.objects.filter(idPoint=self.get_object(), date__range=(starttime, endtime)).order_by(order)
                ts = ReadingTSHour.objects.filter(idPoint=self.get_object(), date__range=(starttime, endtime)).order_by(order)
        # Заполнение итоговых значение (сумма или среднее) для показаний
        gvs_Q1 = gvs.aggregate(Sum('Q1'))
        gvs_Q1_Total = gvs.aggregate(Sum('Q1_Total'))
        gvs_Q2 = gvs.aggregate(Sum('Q2'))
        gvs_Q2_Total = gvs.aggregate(Sum('Q2_Total'))
        gvs_t1 = gvs.aggregate(Avg('t1'))
        gvs_t2 = gvs.aggregate(Avg('t2'))
        gvs_V1 = gvs.aggregate(Sum('V1'))
        gvs_V1_Total = gvs.aggregate(Sum('V1_Total'))
        gvs_V2 = gvs.aggregate(Sum('V2'))
        gvs_V2_Total = gvs.aggregate(Sum('V2_Total'))
        gvs_M1 = gvs.aggregate(Sum('M1'))
        gvs_M1_Total = gvs.aggregate(Sum('M1_Total'))
        gvs_M2 = gvs.aggregate(Sum('M2'))
        gvs_M2_Total = gvs.aggregate(Sum('M2_Total'))
        gvs_P1 = gvs.aggregate(Avg('P1'))
        gvs_P2 = gvs.aggregate(Avg('P2'))
        gvs_Q = gvs.aggregate(Sum('Q'))
        gvs_Q_Total = gvs.aggregate(Sum('Q_Total'))
        gvs_dt = gvs.aggregate(Avg('dt'))
        gvs_dV = gvs.aggregate(Avg('dV'))
        gvs_dM = gvs.aggregate(Avg('dM'))
        gvs_tcw = gvs.aggregate(Avg('tcw'))
        gvs_Pcw = gvs.aggregate(Avg('Pcw'))
        gvs_TGmax = gvs.aggregate(Sum('TGmax'))
        gvs_TGmin = gvs.aggregate(Sum('TGmin'))
        gvs_Tdt = gvs.aggregate(Avg('Tdt'))
        gvs_TFault = gvs.aggregate(Avg('TFault'))
        gvs_Toff = gvs.aggregate(Avg('Toff'))
        gvs_TOtherNS = gvs.aggregate(Sum('TOtherNS'))
        gvs_QntHIP = gvs.aggregate(Sum('QntHIP'))
        gvs_QntHIP_Total = gvs.aggregate(Sum('QntHIP_Total'))
        gvs_QntP = gvs.aggregate(Sum('QntP'))
        gvs_QntP_Total = gvs.aggregate(Sum('QntP_Total'))
        ts_Q1 = ts.aggregate(Sum('Q1'))
        ts_Q1_Total = ts.aggregate(Sum('Q1_Total'))
        ts_Q2 = ts.aggregate(Sum('Q2'))
        ts_Q2_Total = ts.aggregate(Sum('Q2_Total'))
        ts_t1 = ts.aggregate(Avg('t1'))
        ts_t2 = ts.aggregate(Avg('t2'))
        ts_t3 = ts.aggregate(Avg('t3'))
        ts_V1 = ts.aggregate(Sum('V1'))
        ts_V1_Total = ts.aggregate(Sum('V1_Total'))
        ts_V2 = ts.aggregate(Sum('V2'))
        ts_V2_Total = ts.aggregate(Sum('V2_Total'))
        ts_V3 = ts.aggregate(Sum('V3'))
        ts_V3_Total = ts.aggregate(Sum('V3_Total'))
        ts_M1 = ts.aggregate(Sum('M1'))
        ts_M1_Total = ts.aggregate(Sum('M1_Total'))
        ts_M2 = ts.aggregate(Sum('M2'))
        ts_M2_Total = ts.aggregate(Sum('M2_Total'))
        ts_M3 = ts.aggregate(Sum('M3'))
        ts_M3_Total = ts.aggregate(Sum('M3_Total'))
        ts_P1 = ts.aggregate(Avg('P1'))
        ts_P2 = ts.aggregate(Avg('P2'))
        ts_P3 = ts.aggregate(Avg('P3'))
        ts_Q = ts.aggregate(Sum('Q'))
        ts_Q_Total = ts.aggregate(Sum('Q_Total'))
        ts_dt = ts.aggregate(Avg('dt'))
        ts_dV = ts.aggregate(Avg('dV'))
        ts_dM = ts.aggregate(Avg('dM'))
        ts_ta = ts.aggregate(Avg('ta'))
        ts_tcw = ts.aggregate(Avg('tcw'))
        ts_Pcw = ts.aggregate(Avg('Pcw'))
        ts_TGmax = ts.aggregate(Sum('TGmax'))
        ts_TGmin = ts.aggregate(Sum('TGmin'))
        ts_Tdt = ts.aggregate(Avg('Tdt'))
        ts_TFault = ts.aggregate(Avg('TFault'))
        ts_Toff = ts.aggregate(Avg('Toff'))
        ts_TOtherNS = ts.aggregate(Sum('TOtherNS'))
        ts_QntHIP = ts.aggregate(Sum('QntHIP'))
        ts_QntHIP_Total = ts.aggregate(Sum('QntHIP_Total'))
        ts_QntP = ts.aggregate(Sum('QntP'))
        ts_QntP_Total = ts.aggregate(Sum('QntP_Total'))
        context['gvs_Q1'] = gvs_Q1
        context['gvs_Q1_Total'] = gvs_Q1_Total
        context['gvs_Q2'] = gvs_Q2
        context['gvs_Q2_Total'] = gvs_Q2_Total
        context['gvs_t1'] = gvs_t1
        context['gvs_t2'] = gvs_t2
        context['gvs_V1'] = gvs_V1
        context['gvs_V1_Total'] = gvs_V1_Total
        context['gvs_V2'] = gvs_V2
        context['gvs_V2_Total'] = gvs_V2_Total
        context['gvs_M1'] = gvs_M1
        context['gvs_M1_Total'] = gvs_M1_Total
        context['gvs_M2'] = gvs_M2
        context['gvs_M2_Total'] = gvs_M2_Total
        context['gvs_P1'] = gvs_P1
        context['gvs_P2'] = gvs_P2
        context['gvs_Q'] = gvs_Q
        context['gvs_Q_Total'] = gvs_Q_Total
        context['gvs_dt'] = gvs_dt
        context['gvs_dV'] = gvs_dV
        context['gvs_dM'] = gvs_dM
        context['gvs_tcw'] = gvs_tcw
        context['gvs_Pcw'] = gvs_Pcw
        context['gvs_TGmax'] = gvs_TGmax
        context['gvs_TGmin'] = gvs_TGmin
        context['gvs_Tdt'] = gvs_Tdt
        context['gvs_TFault'] = gvs_TFault
        context['gvs_Toff'] = gvs_Toff
        context['gvs_TOtherNS'] = gvs_TOtherNS
        context['gvs_QntHIP'] = gvs_QntHIP
        context['gvs_QntHIP_Total'] = gvs_QntHIP_Total
        context['gvs_QntP'] = gvs_QntP
        context['gvs_QntP_Total'] = gvs_QntP_Total
        context['ts_Q1'] = ts_Q1
        context['ts_Q1_Total'] = ts_Q1_Total
        context['ts_Q2'] = ts_Q2
        context['ts_Q2_Total'] = ts_Q2_Total
        context['ts_t1'] = ts_t1
        context['ts_t2'] = ts_t2
        context['ts_t3'] = ts_t3
        context['ts_V1'] = ts_V1
        context['ts_V1_Total'] = ts_V1_Total
        context['ts_V2'] = ts_V2
        context['ts_V2_Total'] = ts_V2_Total
        context['ts_V3'] = ts_V3
        context['ts_V3_Total'] = ts_V3_Total
        context['ts_M1'] = ts_M1
        context['ts_M1_Total'] = ts_M1_Total
        context['ts_M2'] = ts_M2
        context['ts_M2_Total'] = ts_M2_Total
        context['ts_M3'] = ts_M3
        context['ts_M3_Total'] = ts_M3_Total
        context['ts_P1'] = ts_P1
        context['ts_P2'] = ts_P2
        context['ts_P3'] = ts_P3
        context['ts_Q'] = ts_Q
        context['ts_Q_Total'] = ts_Q_Total
        context['ts_dt'] = ts_dt
        context['ts_dV'] = ts_dV
        context['ts_dM'] = ts_dM
        context['ts_ta'] = ts_ta
        context['ts_tcw'] = ts_tcw
        context['ts_Pcw'] = ts_Pcw
        context['ts_TGmax'] = ts_TGmax
        context['ts_TGmin'] = ts_TGmin
        context['ts_Tdt'] = ts_Tdt
        context['ts_TFault'] = ts_TFault
        context['ts_Toff'] = ts_Toff
        context['ts_TOtherNS'] = ts_TOtherNS
        context['ts_QntHIP'] = ts_QntHIP
        context['ts_QntHIP_Total'] = ts_QntHIP_Total
        context['ts_QntP'] = ts_QntP
        context['ts_QntP_Total'] = ts_QntP_Total
        # Количество записей на странице
        paginator = Paginator(gvs, 31)
        userlogin = User.objects.get(username=self.request.user)
        idUser = CheckParams.objects.get(username_id = userlogin.id)
        context['checkparams'] = idUser
        context['idUser'] = idUser.id
        page_number = self.request.GET.get('page')
        context['gvs'] = paginator.get_page(page_number)
        paginator = Paginator(ts, 31)
        context['ts'] = paginator.get_page(page_number)
        context['startDate'] = startDate
        context['endDate'] = endDate
        context['month'] = month
        context['typeArchive'] = typeArchive
        context['options'] = options
        context['order'] = order
        context['order_list'] = order_list
        context['check'] = check
        context['q'] = search
        if (search == ''):
            context['list'] = ListTU.objects.all().order_by(order_list)
        else:
            context['list'] = ListTU.objects.filter(Q(idObject__tags__name__icontains=search) | Q(nameObject__icontains=search) | Q(addressObject__icontains=search) | Q(modelPU__icontains=search) | Q(numberPU__icontains=search) | Q(point__icontains=search) | Q(resource__icontains=search)).order_by(order_list).distinct()
        return context


# Класс для авторизации
class ConnectLogin(LoginView):
    template_name = 'eldis/login.html'
    form_class = AuthUserForm
    success_url = reverse_lazy('double')
    def get_success_url(self):
        return self.success_url


# Класс для выхода текущего пользователя
class ConnectLogout(LogoutView):
    next_page = reverse_lazy('login_page')


# Класс для выбора отображаемых полей в таблице с показаниями
class UpdateCheckParams(UpdateView):
    model = CheckParams
    template_name = 'eldis/check.html'
    form_class = CheckForm
    success_url = reverse_lazy('double')
    def get_context_data(self, **kwargs):
        return super().get_context_data(**kwargs)
