import requests
from django.db.models import Q
from .models import ListTU, ReadingTSDay, ReadingGVSDay, ReadingGVSHour, ReadingTSHour, Tags, ObjectsEldis
from datetime import datetime, timedelta


# Функция обновления списка тегов, списка адресов и списка точек учета
def update_listPU():
    url = "https://api.eldis24.ru/api/v3/users/login"
    headers = {'Content-Type': 'application/x-www-form-urlencoded', 'key': 'ee831d72eed54a83a2d2e363a03f953c'}
    # Запрос на авторизацию
    res = requests.post(url, headers=headers, data={'login': 'Litvinovmv93@mail.ru', 'password': 'Miha1007!'})
    # Сохранение токена для дальнейших запросов
    cookies = res.cookies['access_token']
    url = "https://api.eldis24.ru/api/v3/objects/list?limit=300"
    # Запрос на получение списка объектов
    res = requests.post(url, headers=headers, cookies={'access_token': cookies}).json()
    i = 0
    # Проход по ответу и создание/обновление данных
    while i < len(res['response']['objects']['list']):
        obj, created = ObjectsEldis.objects.update_or_create(idObject=res['response']['objects']['list'][i]['id'],
            defaults={
                'address': res['response']['objects']['list'][i]['address'],
                'name': res['response']['objects']['list'][i]['name'],
                'consumer': res['response']['objects']['list'][i]['consumer'],
            },
        )
        updateObject = ObjectsEldis.objects.get(pk=res['response']['objects']['list'][i]['id'])
        j = 0
        # Проход по массиву тегов и создание/обновление данных
        while j < len(res['response']['objects']['list'][i]['tags']):
            obj, created = Tags.objects.update_or_create(idTag=res['response']['objects']['list'][i]['tags'][j]['id'],
                defaults={'name': res['response']['objects']['list'][i]['tags'][j]['name']})
            # добавление тэга к текущей записи адреса
            updateObject.tags.add(Tags.objects.get(pk=res['response']['objects']['list'][i]['tags'][j]['id']))
            j = j + 1
        i = i + 1
    url = "https://api.eldis24.ru/api/v3/tv/list?limit=300"
    # Получение списка точек учета
    res = requests.post(url, headers=headers, cookies={'access_token': cookies}).json()
    i = 0
    # Проход по массиву точек учета и создание/обновление данных
    while i < len(res['response']['tv']['list']):
        obj, created = ListTU.objects.update_or_create(idPoint=res['response']['tv']['list'][i]['id'],
            defaults={
                'idObject': ObjectsEldis.objects.get(pk=res['response']['tv']['list'][i]['objectID']),
                'nameObject': res['response']['tv']['list'][i]['objectName'],
                'addressObject': res['response']['tv']['list'][i]['addressObject'],
                'modelPU': res['response']['tv']['list'][i]['deviceModelName'],
                'numberPU': res['response']['tv']['list'][i]['deviceSN'],
                'point': res['response']['tv']['list'][i]['tvName'],
                'resource': res['response']['tv']['list'][i]['resource']
            },
        )
        i = i + 1


# Функция обновления часовых показаний ГВС
def update_gvshour():
    url = "https://api.eldis24.ru/api/v3/users/login"
    headers = {'Content-Type': 'application/x-www-form-urlencoded', 'key': 'ee831d72eed54a83a2d2e363a03f953c'}
    res = requests.post(url, headers=headers, data={'login': 'Litvinovmv93@mail.ru', 'password': 'Miha1007!'})
    cookies = res.cookies['access_token']
    listPU = ListTU.objects.filter(resource='ГВС')
    endDate = datetime.today()
    # Промежуток времени - неделя
    delta = timedelta(weeks=1)
    startDate = endDate - delta
    # Получение данных для каждой точки учета, у которой ресурс = ГВС
    for elem in listPU:
        url = "https://api.eldis24.ru/api/v3/data/normalized?id=" + elem.idPoint + "&typeDataCode=30003&startDate=" + startDate.strftime('%d.%m.%Y %H:%M:%S') + "&endDate=" + endDate.strftime('%d.%m.%Y %H:%M:%S') + "&dateType=date"
        res = requests.get(url, headers=headers, cookies={'access_token': cookies}).json()
        i = 0
        # Проход по массиву с показаниями и создание/обновление данных
        while i < len(res['response']['data']['normalized'][0]['hotWater']):
            obj, created = ReadingGVSHour.objects.update_or_create(
                idPoint_id=elem.idPoint,
                date=datetime.fromtimestamp(res['response']['data']['normalized'][0]['hotWater'][i]['date']),
                defaults={
                    'dateOnEndOfArchive': datetime.fromtimestamp(res['response']['data']['normalized'][0]['hotWater'][i]['dateOnEndOfArchive']),
                    'dateWithTimeBias': datetime.fromtimestamp(res['response']['data']['normalized'][0]['hotWater'][i]['dateWithTimeBias']),
                    'Q1': res['response']['data']['normalized'][0]['hotWater'][i]['Q1'],
                    'Q1_Total': res['response']['data']['normalized'][0]['hotWater'][i]['Q1_Total'],
                    'Q2': res['response']['data']['normalized'][0]['hotWater'][i]['Q2'],
                    'Q2_Total': res['response']['data']['normalized'][0]['hotWater'][i]['Q2_Total'],
                    't1': res['response']['data']['normalized'][0]['hotWater'][i]['t1'],
                    't2': res['response']['data']['normalized'][0]['hotWater'][i]['t2'],
                    'V1': res['response']['data']['normalized'][0]['hotWater'][i]['V1'],
                    'V1_Total': res['response']['data']['normalized'][0]['hotWater'][i]['V1_Total'],
                    'V2': res['response']['data']['normalized'][0]['hotWater'][i]['V2'],
                    'V2_Total': res['response']['data']['normalized'][0]['hotWater'][i]['V2_Total'],
                    'M1': res['response']['data']['normalized'][0]['hotWater'][i]['M1'],
                    'M1_Total': res['response']['data']['normalized'][0]['hotWater'][i]['M1_Total'],
                    'M2': res['response']['data']['normalized'][0]['hotWater'][i]['M2'],
                    'M2_Total': res['response']['data']['normalized'][0]['hotWater'][i]['M2_Total'],
                    'P1': res['response']['data']['normalized'][0]['hotWater'][i]['P1'],
                    'P2': res['response']['data']['normalized'][0]['hotWater'][i]['P2'],
                    'Q': res['response']['data']['normalized'][0]['hotWater'][i]['Q'],
                    'Q_Total': res['response']['data']['normalized'][0]['hotWater'][i]['Q_Total'],
                    'dt': res['response']['data']['normalized'][0]['hotWater'][i]['dt'],
                    'dV': res['response']['data']['normalized'][0]['hotWater'][i]['dV'],
                    'dM': res['response']['data']['normalized'][0]['hotWater'][i]['dM'],
                    'tcw': res['response']['data']['normalized'][0]['hotWater'][i]['tcw'],
                    'Pcw': res['response']['data']['normalized'][0]['hotWater'][i]['Pcw'],
                    'TGmax': res['response']['data']['normalized'][0]['hotWater'][i]['TGmax'],
                    'TGmin': res['response']['data']['normalized'][0]['hotWater'][i]['TGmin'],
                    'Tdt': res['response']['data']['normalized'][0]['hotWater'][i]['Tdt'],
                    'TFault': res['response']['data']['normalized'][0]['hotWater'][i]['TFault'],
                    'Toff': res['response']['data']['normalized'][0]['hotWater'][i]['Toff'],
                    'TOtherNS': res['response']['data']['normalized'][0]['hotWater'][i]['TOtherNS'],
                    'ns': res['response']['data']['normalized'][0]['hotWater'][i]['ns'],
                    'QntHIP': res['response']['data']['normalized'][0]['hotWater'][i]['QntHIP'],
                    'QntHIP_Total': res['response']['data']['normalized'][0]['hotWater'][i]['QntHIP_Total'],
                    'QntP': res['response']['data']['normalized'][0]['hotWater'][i]['QntP'],
                    'QntP_Total': res['response']['data']['normalized'][0]['hotWater'][i]['QntP_Total'],
                    'empty': res['response']['data']['normalized'][0]['hotWater'][i]['empty']
                },
            )
            i = i + 1


# Функция обновления часовых показаний ТС
def update_tshour():
    url = "https://api.eldis24.ru/api/v3/users/login"
    headers = {'Content-Type': 'application/x-www-form-urlencoded', 'key': 'ee831d72eed54a83a2d2e363a03f953c'}
    res = requests.post(url, headers=headers, data={'login': 'Litvinovmv93@mail.ru', 'password': 'Miha1007!'})
    cookies = res.cookies['access_token']
    listPU = ListTU.objects.filter(Q(resource='ТС') | Q(resource='Тепло'))
    endDate = datetime.today()
    delta = timedelta(weeks=1)
    startDate = endDate - delta
    for elem in listPU:
        url = "https://api.eldis24.ru/api/v3/data/normalized?id=" + elem.idPoint + "&typeDataCode=30003&startDate=" + startDate.strftime('%d.%m.%Y %H:%M:%S') + "&endDate=" + endDate.strftime('%d.%m.%Y %H:%M:%S') + "&dateType=date"
        res = requests.get(url, headers=headers, cookies={'access_token': cookies}).json()
        i = 0
        while i < len(res['response']['data']['normalized'][0]['heat']):
            obj, created = ReadingTSHour.objects.update_or_create(
                idPoint_id=elem.idPoint,
                date=datetime.fromtimestamp(res['response']['data']['normalized'][0]['heat'][i]['date']),
                defaults={
                    'dateOnEndOfArchive': datetime.fromtimestamp(res['response']['data']['normalized'][0]['heat'][i]['dateOnEndOfArchive']),
                    'dateWithTimeBias': datetime.fromtimestamp(res['response']['data']['normalized'][0]['heat'][i]['dateWithTimeBias']),
                    'Q1': res['response']['data']['normalized'][0]['heat'][i]['Q1'],
                    'Q1_Total': res['response']['data']['normalized'][0]['heat'][i]['Q1_Total'],
                    'Q2': res['response']['data']['normalized'][0]['heat'][i]['Q2'],
                    'Q2_Total': res['response']['data']['normalized'][0]['heat'][i]['Q2_Total'],
                    't1': res['response']['data']['normalized'][0]['heat'][i]['t1'],
                    't2': res['response']['data']['normalized'][0]['heat'][i]['t2'],
                    't3': res['response']['data']['normalized'][0]['heat'][i]['t3'],
                    'V1': res['response']['data']['normalized'][0]['heat'][i]['V1'],
                    'V1_Total': res['response']['data']['normalized'][0]['heat'][i]['V1_Total'],
                    'V2': res['response']['data']['normalized'][0]['heat'][i]['V2'],
                    'V2_Total': res['response']['data']['normalized'][0]['heat'][i]['V2_Total'],
                    'V3': res['response']['data']['normalized'][0]['heat'][i]['V3'],
                    'V3_Total': res['response']['data']['normalized'][0]['heat'][i]['V3_Total'],
                    'M1': res['response']['data']['normalized'][0]['heat'][i]['M1'],
                    'M1_Total': res['response']['data']['normalized'][0]['heat'][i]['M1_Total'],
                    'M2': res['response']['data']['normalized'][0]['heat'][i]['M2'],
                    'M2_Total': res['response']['data']['normalized'][0]['heat'][i]['M2_Total'],
                    'M3': res['response']['data']['normalized'][0]['heat'][i]['M3'],
                    'M3_Total': res['response']['data']['normalized'][0]['heat'][i]['M3_Total'],
                    'P1': res['response']['data']['normalized'][0]['heat'][i]['P1'],
                    'P2': res['response']['data']['normalized'][0]['heat'][i]['P2'],
                    'P3': res['response']['data']['normalized'][0]['heat'][i]['P3'],
                    'Q': res['response']['data']['normalized'][0]['heat'][i]['Q'],
                    'Q_Total': res['response']['data']['normalized'][0]['heat'][i]['Q_Total'],
                    'dt': res['response']['data']['normalized'][0]['heat'][i]['dt'],
                    'dV': res['response']['data']['normalized'][0]['heat'][i]['dV'],
                    'dM': res['response']['data']['normalized'][0]['heat'][i]['dM'],
                    'ta': res['response']['data']['normalized'][0]['heat'][i]['ta'],
                    'tcw': res['response']['data']['normalized'][0]['heat'][i]['tcw'],
                    'Pcw': res['response']['data']['normalized'][0]['heat'][i]['Pcw'],
                    'TGmax': res['response']['data']['normalized'][0]['heat'][i]['TGmax'],
                    'TGmin': res['response']['data']['normalized'][0]['heat'][i]['TGmin'],
                    'Tdt': res['response']['data']['normalized'][0]['heat'][i]['Tdt'],
                    'TFault': res['response']['data']['normalized'][0]['heat'][i]['TFault'],
                    'Toff': res['response']['data']['normalized'][0]['heat'][i]['Toff'],
                    'TOtherNS': res['response']['data']['normalized'][0]['heat'][i]['TOtherNS'],
                    'ns': res['response']['data']['normalized'][0]['heat'][i]['ns'],
                    'QntHIP': res['response']['data']['normalized'][0]['heat'][i]['QntHIP'],
                    'QntHIP_Total': res['response']['data']['normalized'][0]['heat'][i]['QntHIP_Total'],
                    'QntP': res['response']['data']['normalized'][0]['heat'][i]['QntP'],
                    'QntP_Total': res['response']['data']['normalized'][0]['heat'][i]['QntP_Total'],
                    'empty': res['response']['data']['normalized'][0]['heat'][i]['empty']
                },
            )
            i = i + 1


# Функция обновления суточных показаний ГВС
def update_gvsday():
    url = "https://api.eldis24.ru/api/v3/users/login"
    headers = {'Content-Type': 'application/x-www-form-urlencoded', 'key': 'ee831d72eed54a83a2d2e363a03f953c'}
    res = requests.post(url, headers=headers, data={'login': 'Litvinovmv93@mail.ru', 'password': 'Miha1007!'})
    cookies = res.cookies['access_token']
    listPU = ListTU.objects.filter(resource='ГВС')
    endDate = datetime.today()
    delta = timedelta(weeks=1)
    startDate = endDate - delta
    for elem in listPU:
        url = "https://api.eldis24.ru/api/v3/data/normalized?id=" + elem.idPoint + "&typeDataCode=30004&startDate=" + startDate.strftime('%d.%m.%Y %H:%M:%S') + "&endDate=" + endDate.strftime('%d.%m.%Y %H:%M:%S') + "&dateType=date"
        res = requests.get(url, headers=headers, cookies={'access_token': cookies}).json()
        i = 0
        while i < len(res['response']['data']['normalized'][0]['hotWater']):
            obj, created = ReadingGVSDay.objects.update_or_create(
                idPoint_id=elem.idPoint,
                date=datetime.fromtimestamp(res['response']['data']['normalized'][0]['hotWater'][i]['date']),
                defaults={
                    'dateOnEndOfArchive': datetime.fromtimestamp(res['response']['data']['normalized'][0]['hotWater'][i]['dateOnEndOfArchive']),
                    'dateWithTimeBias': datetime.fromtimestamp(res['response']['data']['normalized'][0]['hotWater'][i]['dateWithTimeBias']),
                    'Q1': res['response']['data']['normalized'][0]['hotWater'][i]['Q1'],
                    'Q1_Total': res['response']['data']['normalized'][0]['hotWater'][i]['Q1_Total'],
                    'Q2': res['response']['data']['normalized'][0]['hotWater'][i]['Q2'],
                    'Q2_Total': res['response']['data']['normalized'][0]['hotWater'][i]['Q2_Total'],
                    't1': res['response']['data']['normalized'][0]['hotWater'][i]['t1'],
                    't2': res['response']['data']['normalized'][0]['hotWater'][i]['t2'],
                    'V1': res['response']['data']['normalized'][0]['hotWater'][i]['V1'],
                    'V1_Total': res['response']['data']['normalized'][0]['hotWater'][i]['V1_Total'],
                    'V2': res['response']['data']['normalized'][0]['hotWater'][i]['V2'],
                    'V2_Total': res['response']['data']['normalized'][0]['hotWater'][i]['V2_Total'],
                    'M1': res['response']['data']['normalized'][0]['hotWater'][i]['M1'],
                    'M1_Total': res['response']['data']['normalized'][0]['hotWater'][i]['M1_Total'],
                    'M2': res['response']['data']['normalized'][0]['hotWater'][i]['M2'],
                    'M2_Total': res['response']['data']['normalized'][0]['hotWater'][i]['M2_Total'],
                    'P1': res['response']['data']['normalized'][0]['hotWater'][i]['P1'],
                    'P2': res['response']['data']['normalized'][0]['hotWater'][i]['P2'],
                    'Q': res['response']['data']['normalized'][0]['hotWater'][i]['Q'],
                    'Q_Total': res['response']['data']['normalized'][0]['hotWater'][i]['Q_Total'],
                    'dt': res['response']['data']['normalized'][0]['hotWater'][i]['dt'],
                    'dV': res['response']['data']['normalized'][0]['hotWater'][i]['dV'],
                    'dM': res['response']['data']['normalized'][0]['hotWater'][i]['dM'],
                    'tcw': res['response']['data']['normalized'][0]['hotWater'][i]['tcw'],
                    'Pcw': res['response']['data']['normalized'][0]['hotWater'][i]['Pcw'],
                    'TGmax': res['response']['data']['normalized'][0]['hotWater'][i]['TGmax'],
                    'TGmin': res['response']['data']['normalized'][0]['hotWater'][i]['TGmin'],
                    'Tdt': res['response']['data']['normalized'][0]['hotWater'][i]['Tdt'],
                    'TFault': res['response']['data']['normalized'][0]['hotWater'][i]['TFault'],
                    'Toff': res['response']['data']['normalized'][0]['hotWater'][i]['Toff'],
                    'TOtherNS': res['response']['data']['normalized'][0]['hotWater'][i]['TOtherNS'],
                    'ns': res['response']['data']['normalized'][0]['hotWater'][i]['ns'],
                    'QntHIP': res['response']['data']['normalized'][0]['hotWater'][i]['QntHIP'],
                    'QntHIP_Total': res['response']['data']['normalized'][0]['hotWater'][i]['QntHIP_Total'],
                    'QntP': res['response']['data']['normalized'][0]['hotWater'][i]['QntP'],
                    'QntP_Total': res['response']['data']['normalized'][0]['hotWater'][i]['QntP_Total'],
                    'empty': res['response']['data']['normalized'][0]['hotWater'][i]['empty']
                },
            )
            i = i + 1


# Функция обновления суточных данных ТС
def update_tsday():
    url = "https://api.eldis24.ru/api/v3/users/login"
    headers = {'Content-Type': 'application/x-www-form-urlencoded', 'key': 'ee831d72eed54a83a2d2e363a03f953c'}
    res = requests.post(url, headers=headers, data={'login': 'Litvinovmv93@mail.ru', 'password': 'Miha1007!'})
    cookies = res.cookies['access_token']
    listPU = ListTU.objects.filter(Q(resource='ТС') | Q(resource='Тепло'))
    endDate = datetime.today()
    delta = timedelta(weeks=1)
    startDate = endDate - delta
    for elem in listPU:
        url = "https://api.eldis24.ru/api/v3/data/normalized?id=" + elem.idPoint + "&typeDataCode=30004&startDate=" + startDate.strftime('%d.%m.%Y %H:%M:%S') + "&endDate=" + endDate.strftime('%d.%m.%Y %H:%M:%S') + "&dateType=date"
        res = requests.get(url, headers=headers, cookies={'access_token': cookies}).json()
        i = 0
        while i < len(res['response']['data']['normalized'][0]['heat']):
            obj, created = ReadingTSDay.objects.update_or_create(
                idPoint_id=elem.idPoint,
                date=datetime.fromtimestamp(res['response']['data']['normalized'][0]['heat'][i]['date']),
                defaults={
                    'dateOnEndOfArchive': datetime.fromtimestamp(res['response']['data']['normalized'][0]['heat'][i]['dateOnEndOfArchive']),
                    'dateWithTimeBias': datetime.fromtimestamp(res['response']['data']['normalized'][0]['heat'][i]['dateWithTimeBias']),
                    'Q1': res['response']['data']['normalized'][0]['heat'][i]['Q1'],
                    'Q1_Total': res['response']['data']['normalized'][0]['heat'][i]['Q1_Total'],
                    'Q2': res['response']['data']['normalized'][0]['heat'][i]['Q2'],
                    'Q2_Total': res['response']['data']['normalized'][0]['heat'][i]['Q2_Total'],
                    't1': res['response']['data']['normalized'][0]['heat'][i]['t1'],
                    't2': res['response']['data']['normalized'][0]['heat'][i]['t2'],
                    't3': res['response']['data']['normalized'][0]['heat'][i]['t3'],
                    'V1': res['response']['data']['normalized'][0]['heat'][i]['V1'],
                    'V1_Total': res['response']['data']['normalized'][0]['heat'][i]['V1_Total'],
                    'V2': res['response']['data']['normalized'][0]['heat'][i]['V2'],
                    'V2_Total': res['response']['data']['normalized'][0]['heat'][i]['V2_Total'],
                    'V3': res['response']['data']['normalized'][0]['heat'][i]['V3'],
                    'V3_Total': res['response']['data']['normalized'][0]['heat'][i]['V3_Total'],
                    'M1': res['response']['data']['normalized'][0]['heat'][i]['M1'],
                    'M1_Total': res['response']['data']['normalized'][0]['heat'][i]['M1_Total'],
                    'M2': res['response']['data']['normalized'][0]['heat'][i]['M2'],
                    'M2_Total': res['response']['data']['normalized'][0]['heat'][i]['M2_Total'],
                    'M3': res['response']['data']['normalized'][0]['heat'][i]['M3'],
                    'M3_Total': res['response']['data']['normalized'][0]['heat'][i]['M3_Total'],
                    'P1': res['response']['data']['normalized'][0]['heat'][i]['P1'],
                    'P2': res['response']['data']['normalized'][0]['heat'][i]['P2'],
                    'P3': res['response']['data']['normalized'][0]['heat'][i]['P3'],
                    'Q': res['response']['data']['normalized'][0]['heat'][i]['Q'],
                    'Q_Total': res['response']['data']['normalized'][0]['heat'][i]['Q_Total'],
                    'dt': res['response']['data']['normalized'][0]['heat'][i]['dt'],
                    'dV': res['response']['data']['normalized'][0]['heat'][i]['dV'],
                    'dM': res['response']['data']['normalized'][0]['heat'][i]['dM'],
                    'ta': res['response']['data']['normalized'][0]['heat'][i]['ta'],
                    'tcw': res['response']['data']['normalized'][0]['heat'][i]['tcw'],
                    'Pcw': res['response']['data']['normalized'][0]['heat'][i]['Pcw'],
                    'TGmax': res['response']['data']['normalized'][0]['heat'][i]['TGmax'],
                    'TGmin': res['response']['data']['normalized'][0]['heat'][i]['TGmin'],
                    'Tdt': res['response']['data']['normalized'][0]['heat'][i]['Tdt'],
                    'TFault': res['response']['data']['normalized'][0]['heat'][i]['TFault'],
                    'Toff': res['response']['data']['normalized'][0]['heat'][i]['Toff'],
                    'TOtherNS': res['response']['data']['normalized'][0]['heat'][i]['TOtherNS'],
                    'ns': res['response']['data']['normalized'][0]['heat'][i]['ns'],
                    'QntHIP': res['response']['data']['normalized'][0]['heat'][i]['QntHIP'],
                    'QntHIP_Total': res['response']['data']['normalized'][0]['heat'][i]['QntHIP_Total'],
                    'QntP': res['response']['data']['normalized'][0]['heat'][i]['QntP'],
                    'QntP_Total': res['response']['data']['normalized'][0]['heat'][i]['QntP_Total'],
                    'empty': res['response']['data']['normalized'][0]['heat'][i]['empty']
                },
            )
            i = i + 1


# Функция обновления часовых показаний для точек учета, где ресурс ГВС и ТС
def update_gvs_and_ts_hour():
    url = "https://api.eldis24.ru/api/v3/users/login"
    headers = {'Content-Type': 'application/x-www-form-urlencoded', 'key': 'ee831d72eed54a83a2d2e363a03f953c'}
    res = requests.post(url, headers=headers, data={'login': 'Litvinovmv93@mail.ru', 'password': 'Miha1007!'})
    cookies = res.cookies['access_token']
    listPU = ListTU.objects.filter(Q(resource='ТС+ГВС (Общий учёт с контролем ГВС)') | Q(resource='Тепло+ГВС'))
    endDate = datetime.today()
    delta = timedelta(weeks=1)
    startDate = endDate - delta
    for elem in listPU:
        url = "https://api.eldis24.ru/api/v3/data/normalized?id=" + elem.idPoint + "&typeDataCode=30003&startDate=" + startDate.strftime('%d.%m.%Y %H:%M:%S') + "&endDate=" + endDate.strftime('%d.%m.%Y %H:%M:%S') + "&dateType=date"
        res = requests.get(url, headers=headers, cookies={'access_token': cookies}).json()
        i = 0
        while i < len(res['response']['data']['normalized'][0]['hotWater']):
            obj, created = ReadingGVSHour.objects.update_or_create(
                idPoint_id=elem.idPoint,
                date=datetime.fromtimestamp(res['response']['data']['normalized'][0]['hotWater'][i]['date']),
                defaults={
                    'dateOnEndOfArchive': datetime.fromtimestamp(res['response']['data']['normalized'][0]['hotWater'][i]['dateOnEndOfArchive']),
                    'dateWithTimeBias': datetime.fromtimestamp(res['response']['data']['normalized'][0]['hotWater'][i]['dateWithTimeBias']),
                    'Q1': res['response']['data']['normalized'][0]['hotWater'][i]['Q1'],
                    'Q1_Total': res['response']['data']['normalized'][0]['hotWater'][i]['Q1_Total'],
                    'Q2': res['response']['data']['normalized'][0]['hotWater'][i]['Q2'],
                    'Q2_Total': res['response']['data']['normalized'][0]['hotWater'][i]['Q2_Total'],
                    't1': res['response']['data']['normalized'][0]['hotWater'][i]['t1'],
                    't2': res['response']['data']['normalized'][0]['hotWater'][i]['t2'],
                    'V1': res['response']['data']['normalized'][0]['hotWater'][i]['V1'],
                    'V1_Total': res['response']['data']['normalized'][0]['hotWater'][i]['V1_Total'],
                    'V2': res['response']['data']['normalized'][0]['hotWater'][i]['V2'],
                    'V2_Total': res['response']['data']['normalized'][0]['hotWater'][i]['V2_Total'],
                    'M1': res['response']['data']['normalized'][0]['hotWater'][i]['M1'],
                    'M1_Total': res['response']['data']['normalized'][0]['hotWater'][i]['M1_Total'],
                    'M2': res['response']['data']['normalized'][0]['hotWater'][i]['M2'],
                    'M2_Total': res['response']['data']['normalized'][0]['hotWater'][i]['M2_Total'],
                    'P1': res['response']['data']['normalized'][0]['hotWater'][i]['P1'],
                    'P2': res['response']['data']['normalized'][0]['hotWater'][i]['P2'],
                    'Q': res['response']['data']['normalized'][0]['hotWater'][i]['Q'],
                    'Q_Total': res['response']['data']['normalized'][0]['hotWater'][i]['Q_Total'],
                    'dt': res['response']['data']['normalized'][0]['hotWater'][i]['dt'],
                    'dV': res['response']['data']['normalized'][0]['hotWater'][i]['dV'],
                    'dM': res['response']['data']['normalized'][0]['hotWater'][i]['dM'],
                    'tcw': res['response']['data']['normalized'][0]['hotWater'][i]['tcw'],
                    'Pcw': res['response']['data']['normalized'][0]['hotWater'][i]['Pcw'],
                    'TGmax': res['response']['data']['normalized'][0]['hotWater'][i]['TGmax'],
                    'TGmin': res['response']['data']['normalized'][0]['hotWater'][i]['TGmin'],
                    'Tdt': res['response']['data']['normalized'][0]['hotWater'][i]['Tdt'],
                    'TFault': res['response']['data']['normalized'][0]['hotWater'][i]['TFault'],
                    'Toff': res['response']['data']['normalized'][0]['hotWater'][i]['Toff'],
                    'TOtherNS': res['response']['data']['normalized'][0]['hotWater'][i]['TOtherNS'],
                    'ns': res['response']['data']['normalized'][0]['hotWater'][i]['ns'],
                    'QntHIP': res['response']['data']['normalized'][0]['hotWater'][i]['QntHIP'],
                    'QntHIP_Total': res['response']['data']['normalized'][0]['hotWater'][i]['QntHIP_Total'],
                    'QntP': res['response']['data']['normalized'][0]['hotWater'][i]['QntP'],
                    'QntP_Total': res['response']['data']['normalized'][0]['hotWater'][i]['QntP_Total'],
                    'empty': res['response']['data']['normalized'][0]['hotWater'][i]['empty']
                },
            )
            i = i + 1
        i = 0
        while i < len(res['response']['data']['normalized'][1]['heat']):
            obj, created = ReadingTSHour.objects.update_or_create(
                idPoint_id=elem.idPoint,
                date=datetime.fromtimestamp(res['response']['data']['normalized'][1]['heat'][i]['date']),
                defaults={
                    'dateOnEndOfArchive': datetime.fromtimestamp(res['response']['data']['normalized'][1]['heat'][i]['dateOnEndOfArchive']),
                    'dateWithTimeBias': datetime.fromtimestamp(res['response']['data']['normalized'][1]['heat'][i]['dateWithTimeBias']),
                    'Q1': res['response']['data']['normalized'][1]['heat'][i]['Q1'],
                    'Q1_Total': res['response']['data']['normalized'][1]['heat'][i]['Q1_Total'],
                    'Q2': res['response']['data']['normalized'][1]['heat'][i]['Q2'],
                    'Q2_Total': res['response']['data']['normalized'][1]['heat'][i]['Q2_Total'],
                    't1': res['response']['data']['normalized'][1]['heat'][i]['t1'],
                    't2': res['response']['data']['normalized'][1]['heat'][i]['t2'],
                    't3': res['response']['data']['normalized'][1]['heat'][i]['t3'],
                    'V1': res['response']['data']['normalized'][1]['heat'][i]['V1'],
                    'V1_Total': res['response']['data']['normalized'][1]['heat'][i]['V1_Total'],
                    'V2': res['response']['data']['normalized'][1]['heat'][i]['V2'],
                    'V2_Total': res['response']['data']['normalized'][1]['heat'][i]['V2_Total'],
                    'V3': res['response']['data']['normalized'][1]['heat'][i]['V3'],
                    'V3_Total': res['response']['data']['normalized'][1]['heat'][i]['V3_Total'],
                    'M1': res['response']['data']['normalized'][1]['heat'][i]['M1'],
                    'M1_Total': res['response']['data']['normalized'][1]['heat'][i]['M1_Total'],
                    'M2': res['response']['data']['normalized'][1]['heat'][i]['M2'],
                    'M2_Total': res['response']['data']['normalized'][1]['heat'][i]['M2_Total'],
                    'M3': res['response']['data']['normalized'][1]['heat'][i]['M3'],
                    'M3_Total': res['response']['data']['normalized'][1]['heat'][i]['M3_Total'],
                    'P1': res['response']['data']['normalized'][1]['heat'][i]['P1'],
                    'P2': res['response']['data']['normalized'][1]['heat'][i]['P2'],
                    'P3': res['response']['data']['normalized'][1]['heat'][i]['P3'],
                    'Q': res['response']['data']['normalized'][1]['heat'][i]['Q'],
                    'Q_Total': res['response']['data']['normalized'][1]['heat'][i]['Q_Total'],
                    'dt': res['response']['data']['normalized'][1]['heat'][i]['dt'],
                    'dV': res['response']['data']['normalized'][1]['heat'][i]['dV'],
                    'dM': res['response']['data']['normalized'][1]['heat'][i]['dM'],
                    'ta': res['response']['data']['normalized'][1]['heat'][i]['ta'],
                    'tcw': res['response']['data']['normalized'][1]['heat'][i]['tcw'],
                    'Pcw': res['response']['data']['normalized'][1]['heat'][i]['Pcw'],
                    'TGmax': res['response']['data']['normalized'][1]['heat'][i]['TGmax'],
                    'TGmin': res['response']['data']['normalized'][1]['heat'][i]['TGmin'],
                    'Tdt': res['response']['data']['normalized'][1]['heat'][i]['Tdt'],
                    'TFault': res['response']['data']['normalized'][1]['heat'][i]['TFault'],
                    'Toff': res['response']['data']['normalized'][1]['heat'][i]['Toff'],
                    'TOtherNS': res['response']['data']['normalized'][1]['heat'][i]['TOtherNS'],
                    'ns': res['response']['data']['normalized'][1]['heat'][i]['ns'],
                    'QntHIP': res['response']['data']['normalized'][1]['heat'][i]['QntHIP'],
                    'QntHIP_Total': res['response']['data']['normalized'][1]['heat'][i]['QntHIP_Total'],
                    'QntP': res['response']['data']['normalized'][1]['heat'][i]['QntP'],
                    'QntP_Total': res['response']['data']['normalized'][1]['heat'][i]['QntP_Total'],
                    'empty': res['response']['data']['normalized'][1]['heat'][i]['empty']
                },
            )
            i = i + 1


# Функция обновления суточных показаний для точек учета, где ресурс ГВС и ТС
def update_gvs_and_ts_day():
    url = "https://api.eldis24.ru/api/v3/users/login"
    headers = {'Content-Type': 'application/x-www-form-urlencoded', 'key': 'ee831d72eed54a83a2d2e363a03f953c'}
    res = requests.post(url, headers=headers, data={'login': 'Litvinovmv93@mail.ru', 'password': 'Miha1007!'})
    cookies = res.cookies['access_token']
    listPU = ListTU.objects.filter(Q(resource='ТС+ГВС (Общий учёт с контролем ГВС)') | Q(resource='Тепло+ГВС'))
    endDate = datetime.today()
    delta = timedelta(weeks=1)
    startDate = endDate - delta
    for elem in listPU:
        url = "https://api.eldis24.ru/api/v3/data/normalized?id=" + elem.idPoint + "&typeDataCode=30004&startDate=" + startDate.strftime('%d.%m.%Y %H:%M:%S') + "&endDate=" + endDate.strftime('%d.%m.%Y %H:%M:%S') + "&dateType=date"
        res = requests.get(url, headers=headers, cookies={'access_token': cookies}).json()
        i = 0
        while i < len(res['response']['data']['normalized'][0]['hotWater']):
            obj, created = ReadingGVSDay.objects.update_or_create(
                idPoint_id=elem.idPoint,
                date=datetime.fromtimestamp(res['response']['data']['normalized'][0]['hotWater'][i]['date']),
                defaults={
                    'dateOnEndOfArchive': datetime.fromtimestamp(res['response']['data']['normalized'][0]['hotWater'][i]['dateOnEndOfArchive']),
                    'dateWithTimeBias': datetime.fromtimestamp(res['response']['data']['normalized'][0]['hotWater'][i]['dateWithTimeBias']),
                    'Q1': res['response']['data']['normalized'][0]['hotWater'][i]['Q1'],
                    'Q1_Total': res['response']['data']['normalized'][0]['hotWater'][i]['Q1_Total'],
                    'Q2': res['response']['data']['normalized'][0]['hotWater'][i]['Q2'],
                    'Q2_Total': res['response']['data']['normalized'][0]['hotWater'][i]['Q2_Total'],
                    't1': res['response']['data']['normalized'][0]['hotWater'][i]['t1'],
                    't2': res['response']['data']['normalized'][0]['hotWater'][i]['t2'],
                    'V1': res['response']['data']['normalized'][0]['hotWater'][i]['V1'],
                    'V1_Total': res['response']['data']['normalized'][0]['hotWater'][i]['V1_Total'],
                    'V2': res['response']['data']['normalized'][0]['hotWater'][i]['V2'],
                    'V2_Total': res['response']['data']['normalized'][0]['hotWater'][i]['V2_Total'],
                    'M1': res['response']['data']['normalized'][0]['hotWater'][i]['M1'],
                    'M1_Total': res['response']['data']['normalized'][0]['hotWater'][i]['M1_Total'],
                    'M2': res['response']['data']['normalized'][0]['hotWater'][i]['M2'],
                    'M2_Total': res['response']['data']['normalized'][0]['hotWater'][i]['M2_Total'],
                    'P1': res['response']['data']['normalized'][0]['hotWater'][i]['P1'],
                    'P2': res['response']['data']['normalized'][0]['hotWater'][i]['P2'],
                    'Q': res['response']['data']['normalized'][0]['hotWater'][i]['Q'],
                    'Q_Total': res['response']['data']['normalized'][0]['hotWater'][i]['Q_Total'],
                    'dt': res['response']['data']['normalized'][0]['hotWater'][i]['dt'],
                    'dV': res['response']['data']['normalized'][0]['hotWater'][i]['dV'],
                    'dM': res['response']['data']['normalized'][0]['hotWater'][i]['dM'],
                    'tcw': res['response']['data']['normalized'][0]['hotWater'][i]['tcw'],
                    'Pcw': res['response']['data']['normalized'][0]['hotWater'][i]['Pcw'],
                    'TGmax': res['response']['data']['normalized'][0]['hotWater'][i]['TGmax'],
                    'TGmin': res['response']['data']['normalized'][0]['hotWater'][i]['TGmin'],
                    'Tdt': res['response']['data']['normalized'][0]['hotWater'][i]['Tdt'],
                    'TFault': res['response']['data']['normalized'][0]['hotWater'][i]['TFault'],
                    'Toff': res['response']['data']['normalized'][0]['hotWater'][i]['Toff'],
                    'TOtherNS': res['response']['data']['normalized'][0]['hotWater'][i]['TOtherNS'],
                    'ns': res['response']['data']['normalized'][0]['hotWater'][i]['ns'],
                    'QntHIP': res['response']['data']['normalized'][0]['hotWater'][i]['QntHIP'],
                    'QntHIP_Total': res['response']['data']['normalized'][0]['hotWater'][i]['QntHIP_Total'],
                    'QntP': res['response']['data']['normalized'][0]['hotWater'][i]['QntP'],
                    'QntP_Total': res['response']['data']['normalized'][0]['hotWater'][i]['QntP_Total'],
                    'empty': res['response']['data']['normalized'][0]['hotWater'][i]['empty']
                },
            )
            i = i + 1
        i = 0
        while i < len(res['response']['data']['normalized'][1]['heat']):
            obj, created = ReadingTSDay.objects.update_or_create(
                idPoint_id=elem.idPoint,
                date=datetime.fromtimestamp(res['response']['data']['normalized'][1]['heat'][i]['date']),
                defaults={
                    'dateOnEndOfArchive': datetime.fromtimestamp(res['response']['data']['normalized'][1]['heat'][i]['dateOnEndOfArchive']),
                    'dateWithTimeBias': datetime.fromtimestamp(res['response']['data']['normalized'][1]['heat'][i]['dateWithTimeBias']),
                    'Q1': res['response']['data']['normalized'][1]['heat'][i]['Q1'],
                    'Q1_Total': res['response']['data']['normalized'][1]['heat'][i]['Q1_Total'],
                    'Q2': res['response']['data']['normalized'][1]['heat'][i]['Q2'],
                    'Q2_Total': res['response']['data']['normalized'][1]['heat'][i]['Q2_Total'],
                    't1': res['response']['data']['normalized'][1]['heat'][i]['t1'],
                    't2': res['response']['data']['normalized'][1]['heat'][i]['t2'],
                    't3': res['response']['data']['normalized'][1]['heat'][i]['t3'],
                    'V1': res['response']['data']['normalized'][1]['heat'][i]['V1'],
                    'V1_Total': res['response']['data']['normalized'][1]['heat'][i]['V1_Total'],
                    'V2': res['response']['data']['normalized'][1]['heat'][i]['V2'],
                    'V2_Total': res['response']['data']['normalized'][1]['heat'][i]['V2_Total'],
                    'V3': res['response']['data']['normalized'][1]['heat'][i]['V3'],
                    'V3_Total': res['response']['data']['normalized'][1]['heat'][i]['V3_Total'],
                    'M1': res['response']['data']['normalized'][1]['heat'][i]['M1'],
                    'M1_Total': res['response']['data']['normalized'][1]['heat'][i]['M1_Total'],
                    'M2': res['response']['data']['normalized'][1]['heat'][i]['M2'],
                    'M2_Total': res['response']['data']['normalized'][1]['heat'][i]['M2_Total'],
                    'M3': res['response']['data']['normalized'][1]['heat'][i]['M3'],
                    'M3_Total': res['response']['data']['normalized'][1]['heat'][i]['M3_Total'],
                    'P1': res['response']['data']['normalized'][1]['heat'][i]['P1'],
                    'P2': res['response']['data']['normalized'][1]['heat'][i]['P2'],
                    'P3': res['response']['data']['normalized'][1]['heat'][i]['P3'],
                    'Q': res['response']['data']['normalized'][1]['heat'][i]['Q'],
                    'Q_Total': res['response']['data']['normalized'][1]['heat'][i]['Q_Total'],
                    'dt': res['response']['data']['normalized'][1]['heat'][i]['dt'],
                    'dV': res['response']['data']['normalized'][1]['heat'][i]['dV'],
                    'dM': res['response']['data']['normalized'][1]['heat'][i]['dM'],
                    'ta': res['response']['data']['normalized'][1]['heat'][i]['ta'],
                    'tcw': res['response']['data']['normalized'][1]['heat'][i]['tcw'],
                    'Pcw': res['response']['data']['normalized'][1]['heat'][i]['Pcw'],
                    'TGmax': res['response']['data']['normalized'][1]['heat'][i]['TGmax'],
                    'TGmin': res['response']['data']['normalized'][1]['heat'][i]['TGmin'],
                    'Tdt': res['response']['data']['normalized'][1]['heat'][i]['Tdt'],
                    'TFault': res['response']['data']['normalized'][1]['heat'][i]['TFault'],
                    'Toff': res['response']['data']['normalized'][1]['heat'][i]['Toff'],
                    'TOtherNS': res['response']['data']['normalized'][1]['heat'][i]['TOtherNS'],
                    'ns': res['response']['data']['normalized'][1]['heat'][i]['ns'],
                    'QntHIP': res['response']['data']['normalized'][1]['heat'][i]['QntHIP'],
                    'QntHIP_Total': res['response']['data']['normalized'][1]['heat'][i]['QntHIP_Total'],
                    'QntP': res['response']['data']['normalized'][1]['heat'][i]['QntP'],
                    'QntP_Total': res['response']['data']['normalized'][1]['heat'][i]['QntP_Total'],
                    'empty': res['response']['data']['normalized'][1]['heat'][i]['empty']
                },
            )
            i = i + 1
