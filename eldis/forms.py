from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User
from .models import CheckParams


# Форма аутентификации пользователя
class AuthUserForm(AuthenticationForm, forms.ModelForm):
    class Meta:
        model = User
        fields = ('username', 'password')


# Форма для отображаемых столбцов в таблице с показаниями
class CheckForm(forms.ModelForm):
    class Meta:
        model = CheckParams
        fields = '__all__'