from django.apps import AppConfig


class EldisConfig(AppConfig):
    name = 'eldis'
