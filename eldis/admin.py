from django.contrib import admin
from .models import ListTU, ReadingGVSDay, ReadingTSDay, ReadingGVSHour, ReadingTSHour, CheckParams, ObjectsEldis, Tags


# Отображение моделей в админке (interbus.omskrts.ru/admin)
admin.site.register(Tags)
admin.site.register(ObjectsEldis)
admin.site.register(ListTU)
admin.site.register(ReadingGVSDay)
admin.site.register(ReadingTSDay)
admin.site.register(ReadingGVSHour)
admin.site.register(ReadingTSHour)
admin.site.register(CheckParams)
