from django.db import models
from django.contrib.auth.models import User


# Модель тэгов
class Tags(models.Model):
    idTag = models.CharField("id тэга", primary_key=True, max_length=200)
    name = models.CharField("Название тэга", max_length=500, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Тэг"
        verbose_name_plural = "Тэги"


# Модель объектов с адресами, тэгами и потребителями
class ObjectsEldis(models.Model):
    idObject = models.CharField("id объекта", primary_key=True, max_length=200)
    address = models.CharField("Адрес объекта", max_length=500, null=True)
    name = models.CharField("Наименование объекта", max_length=500, null=True)
    consumer = models.CharField("Потребитель", max_length=1000, null=True)
    tags = models.ManyToManyField(Tags)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Адрес объекта"
        verbose_name_plural = "Адреса объектов"


# Модель точек учета
class ListTU(models.Model):
    idObject = models.ForeignKey(ObjectsEldis, on_delete=models.CASCADE, verbose_name="id объекта", blank=True, null=True)
    nameObject = models.CharField("Наименование объекта", max_length=500, null=True)
    addressObject = models.CharField("Адрес объекта", max_length=500, null=True)
    modelPU = models.CharField("Модель ПУ", max_length=200, null=True)
    numberPU = models.CharField("Номер ПУ", max_length=200, null=True)
    point = models.CharField("Точка учета", max_length=200, null=True)
    idPoint = models.CharField("id точки учета", primary_key=True, max_length=200)
    resource = models.CharField("Ресурс", max_length=200, blank=True, null=True)

    def __str__(self):
        return self.idPoint

    class Meta:
        verbose_name = "Точка учета"
        verbose_name_plural = "Точки учета"


# Модель суточных показаний ГВС
class ReadingGVSDay(models.Model):
    idPoint = models.ForeignKey(ListTU, on_delete=models.CASCADE, verbose_name="id точки учета", blank=True, null=True)
    date = models.DateTimeField(verbose_name="date", blank=True, null=True)
    dateOnEndOfArchive = models.DateTimeField(verbose_name="dateOnEndOfArchive", blank=True, null=True)
    dateWithTimeBias = models.DateTimeField(verbose_name="dateWithTimeBias", blank=True, null=True)
    Q1 = models.FloatField(verbose_name="Q1", blank=True, null=True)
    Q1_Total = models.FloatField(verbose_name="Q1_Total", blank=True, null=True)
    Q2 = models.FloatField(verbose_name="Q2", blank=True, null=True)
    Q2_Total = models.FloatField(verbose_name="Q2_Total", blank=True, null=True)
    t1 = models.FloatField(verbose_name="t1", blank=True, null=True)
    t2 = models.FloatField(verbose_name="t2", blank=True, null=True)
    V1 = models.FloatField(verbose_name="V1", blank=True, null=True)
    V1_Total = models.FloatField(verbose_name="V1_Total", blank=True, null=True)
    V2 = models.FloatField(verbose_name="V2", blank=True, null=True)
    V2_Total = models.FloatField(verbose_name="V2_Total", blank=True, null=True)
    M1 = models.FloatField(verbose_name="M1", blank=True, null=True)
    M1_Total = models.FloatField(verbose_name="M1_Total", blank=True, null=True)
    M2 = models.FloatField(verbose_name="M2", blank=True, null=True)
    M2_Total = models.FloatField(verbose_name="M2_Total", blank=True, null=True)
    P1 = models.FloatField(verbose_name="P1", blank=True, null=True)
    P2 = models.FloatField(verbose_name="P2", blank=True, null=True)
    Q = models.FloatField(verbose_name="Q", blank=True, null=True)
    Q_Total = models.FloatField(verbose_name="Q_Total", blank=True, null=True)
    dt = models.FloatField(verbose_name="dt", blank=True, null=True)
    dV = models.FloatField(verbose_name="dV", blank=True, null=True)
    dM = models.FloatField(verbose_name="dM", blank=True, null=True)
    tcw = models.FloatField(verbose_name="tcw", blank=True, null=True)
    Pcw = models.FloatField(verbose_name="Pcw", blank=True, null=True)
    TGmax = models.FloatField(verbose_name="TGmax", blank=True, null=True)
    TGmin = models.FloatField(verbose_name="TGmin", blank=True, null=True)
    Tdt = models.FloatField(verbose_name="Tdt", blank=True, null=True)
    TFault = models.FloatField(verbose_name="TFault", blank=True, null=True)
    Toff = models.FloatField(verbose_name="Toff", blank=True, null=True)
    TOtherNS = models.FloatField(verbose_name="TOtherNS", blank=True, null=True)
    QntHIP = models.FloatField(verbose_name="QntHIP", blank=True, null=True)
    QntHIP_Total = models.FloatField(verbose_name="QntHIP_Total", blank=True, null=True)
    QntP = models.FloatField(verbose_name="QntP", blank=True, null=True)
    QntP_Total = models.FloatField(verbose_name="QntP_Total", blank=True, null=True)
    ns = models.BooleanField(verbose_name="ns", blank=True, null=True)
    empty = models.BooleanField(verbose_name="empty", blank=True, null=True)

    def __str__(self):
        return f"{self.idPoint} - {self.date} - ГВС - суточное"

    class Meta:
        verbose_name = "Показание ГВС - суточное"
        verbose_name_plural = "Показания ГВС - суточные"


# Модель часовых показаний ГВС
class ReadingGVSHour(models.Model):
    idPoint = models.ForeignKey(ListTU, on_delete=models.CASCADE, verbose_name="id точки учета", blank=True, null=True)
    date = models.DateTimeField(verbose_name="date", blank=True, null=True)
    dateOnEndOfArchive = models.DateTimeField(verbose_name="dateOnEndOfArchive", blank=True, null=True)
    dateWithTimeBias = models.DateTimeField(verbose_name="dateWithTimeBias", blank=True, null=True)
    Q1 = models.FloatField(verbose_name="Q1", blank=True, null=True)
    Q1_Total = models.FloatField(verbose_name="Q1_Total", blank=True, null=True)
    Q2 = models.FloatField(verbose_name="Q2", blank=True, null=True)
    Q2_Total = models.FloatField(verbose_name="Q2_Total", blank=True, null=True)
    t1 = models.FloatField(verbose_name="t1", blank=True, null=True)
    t2 = models.FloatField(verbose_name="t2", blank=True, null=True)
    V1 = models.FloatField(verbose_name="V1", blank=True, null=True)
    V1_Total = models.FloatField(verbose_name="V1_Total", blank=True, null=True)
    V2 = models.FloatField(verbose_name="V2", blank=True, null=True)
    V2_Total = models.FloatField(verbose_name="V2_Total", blank=True, null=True)
    M1 = models.FloatField(verbose_name="M1", blank=True, null=True)
    M1_Total = models.FloatField(verbose_name="M1_Total", blank=True, null=True)
    M2 = models.FloatField(verbose_name="M2", blank=True, null=True)
    M2_Total = models.FloatField(verbose_name="M2_Total", blank=True, null=True)
    P1 = models.FloatField(verbose_name="P1", blank=True, null=True)
    P2 = models.FloatField(verbose_name="P2", blank=True, null=True)
    Q = models.FloatField(verbose_name="Q", blank=True, null=True)
    Q_Total = models.FloatField(verbose_name="Q_Total", blank=True, null=True)
    dt = models.FloatField(verbose_name="dt", blank=True, null=True)
    dV = models.FloatField(verbose_name="dV", blank=True, null=True)
    dM = models.FloatField(verbose_name="dM", blank=True, null=True)
    tcw = models.FloatField(verbose_name="tcw", blank=True, null=True)
    Pcw = models.FloatField(verbose_name="Pcw", blank=True, null=True)
    TGmax = models.FloatField(verbose_name="TGmax", blank=True, null=True)
    TGmin = models.FloatField(verbose_name="TGmin", blank=True, null=True)
    Tdt = models.FloatField(verbose_name="Tdt", blank=True, null=True)
    TFault = models.FloatField(verbose_name="TFault", blank=True, null=True)
    Toff = models.FloatField(verbose_name="Toff", blank=True, null=True)
    TOtherNS = models.FloatField(verbose_name="TOtherNS", blank=True, null=True)
    QntHIP = models.FloatField(verbose_name="QntHIP", blank=True, null=True)
    QntHIP_Total = models.FloatField(verbose_name="QntHIP_Total", blank=True, null=True)
    QntP = models.FloatField(verbose_name="QntP", blank=True, null=True)
    QntP_Total = models.FloatField(verbose_name="QntP_Total", blank=True, null=True)
    ns = models.BooleanField(verbose_name="ns", blank=True, null=True)
    empty = models.BooleanField(verbose_name="empty", blank=True, null=True)

    def __str__(self):
        return f"{self.idPoint} - {self.date} - ГВС - часовое"

    class Meta:
        verbose_name = "Показание ГВС - часовое"
        verbose_name_plural = "Показания ГВС - часовые"


# Модель суточных показаний ТС
class ReadingTSDay(models.Model):
    idPoint = models.ForeignKey(ListTU, on_delete=models.CASCADE, verbose_name="id точки учета", blank=True, null=True)
    date = models.DateTimeField(verbose_name="date", blank=True, null=True)
    dateOnEndOfArchive = models.DateTimeField(verbose_name="dateOnEndOfArchive", blank=True, null=True)
    dateWithTimeBias = models.DateTimeField(verbose_name="dateWithTimeBias", blank=True, null=True)
    Q1 = models.FloatField(verbose_name="Q1", blank=True, null=True)
    Q1_Total = models.FloatField(verbose_name="Q1_Total", blank=True, null=True)
    Q2 = models.FloatField(verbose_name="Q2", blank=True, null=True)
    Q2_Total = models.FloatField(verbose_name="Q2_Total", blank=True, null=True)
    t1 = models.FloatField(verbose_name="t1", blank=True, null=True)
    t2 = models.FloatField(verbose_name="t2", blank=True, null=True)
    t3 = models.FloatField(verbose_name="t3", blank=True, null=True)
    V1 = models.FloatField(verbose_name="V1", blank=True, null=True)
    V1_Total = models.FloatField(verbose_name="V1_Total", blank=True, null=True)
    V2 = models.FloatField(verbose_name="V2", blank=True, null=True)
    V2_Total = models.FloatField(verbose_name="V2_Total", blank=True, null=True)
    V3 = models.FloatField(verbose_name="V3", blank=True, null=True)
    V3_Total = models.FloatField(verbose_name="V3_Total", blank=True, null=True)
    M1 = models.FloatField(verbose_name="M1", blank=True, null=True)
    M1_Total = models.FloatField(verbose_name="M1_Total", blank=True, null=True)
    M2 = models.FloatField(verbose_name="M2", blank=True, null=True)
    M2_Total = models.FloatField(verbose_name="M2_Total", blank=True, null=True)
    M3 = models.FloatField(verbose_name="M3", blank=True, null=True)
    M3_Total = models.FloatField(verbose_name="M3_Total", blank=True, null=True)
    P1 = models.FloatField(verbose_name="P1", blank=True, null=True)
    P2 = models.FloatField(verbose_name="P2", blank=True, null=True)
    P3 = models.FloatField(verbose_name="P3", blank=True, null=True)
    Q = models.FloatField(verbose_name="Q", blank=True, null=True)
    Q_Total = models.FloatField(verbose_name="Q_Total", blank=True, null=True)
    dt = models.FloatField(verbose_name="dt", blank=True, null=True)
    dV = models.FloatField(verbose_name="dV", blank=True, null=True)
    dM = models.FloatField(verbose_name="dM", blank=True, null=True)
    ta = models.FloatField(verbose_name="ta", blank=True, null=True)
    tcw = models.FloatField(verbose_name="tcw", blank=True, null=True)
    Pcw = models.FloatField(verbose_name="Pcw", blank=True, null=True)
    TGmax = models.FloatField(verbose_name="TGmax", blank=True, null=True)
    TGmin = models.FloatField(verbose_name="TGmin", blank=True, null=True)
    Tdt = models.FloatField(verbose_name="Tdt", blank=True, null=True)
    TFault = models.FloatField(verbose_name="TFault", blank=True, null=True)
    Toff = models.FloatField(verbose_name="Toff", blank=True, null=True)
    TOtherNS = models.FloatField(verbose_name="TOtherNS", blank=True, null=True)
    QntHIP = models.FloatField(verbose_name="QntHIP", blank=True, null=True)
    QntHIP_Total = models.FloatField(verbose_name="QntHIP_Total", blank=True, null=True)
    QntP = models.FloatField(verbose_name="QntP", blank=True, null=True)
    QntP_Total = models.FloatField(verbose_name="QntP_Total", blank=True, null=True)
    ns = models.BooleanField(verbose_name="ns", blank=True, null=True)
    empty = models.BooleanField(verbose_name="empty", blank=True, null=True)

    def __str__(self):
        return f"{self.idPoint} - {self.date} - ТС - суточное"

    class Meta:
        verbose_name = "Показание ТС - суточное"
        verbose_name_plural = "Показания ТС - суточные"


# Модель часовых показаний ТС
class ReadingTSHour(models.Model):
    idPoint = models.ForeignKey(ListTU, on_delete=models.CASCADE, verbose_name="id точки учета", blank=True, null=True)
    date = models.DateTimeField(verbose_name="date", blank=True, null=True)
    dateOnEndOfArchive = models.DateTimeField(verbose_name="dateOnEndOfArchive", blank=True, null=True)
    dateWithTimeBias = models.DateTimeField(verbose_name="dateWithTimeBias", blank=True, null=True)
    Q1 = models.FloatField(verbose_name="Q1", blank=True, null=True)
    Q1_Total = models.FloatField(verbose_name="Q1_Total", blank=True, null=True)
    Q2 = models.FloatField(verbose_name="Q2", blank=True, null=True)
    Q2_Total = models.FloatField(verbose_name="Q2_Total", blank=True, null=True)
    t1 = models.FloatField(verbose_name="t1", blank=True, null=True)
    t2 = models.FloatField(verbose_name="t2", blank=True, null=True)
    t3 = models.FloatField(verbose_name="t3", blank=True, null=True)
    V1 = models.FloatField(verbose_name="V1", blank=True, null=True)
    V1_Total = models.FloatField(verbose_name="V1_Total", blank=True, null=True)
    V2 = models.FloatField(verbose_name="V2", blank=True, null=True)
    V2_Total = models.FloatField(verbose_name="V2_Total", blank=True, null=True)
    V3 = models.FloatField(verbose_name="V3", blank=True, null=True)
    V3_Total = models.FloatField(verbose_name="V3_Total", blank=True, null=True)
    M1 = models.FloatField(verbose_name="M1", blank=True, null=True)
    M1_Total = models.FloatField(verbose_name="M1_Total", blank=True, null=True)
    M2 = models.FloatField(verbose_name="M2", blank=True, null=True)
    M2_Total = models.FloatField(verbose_name="M2_Total", blank=True, null=True)
    M3 = models.FloatField(verbose_name="M3", blank=True, null=True)
    M3_Total = models.FloatField(verbose_name="M3_Total", blank=True, null=True)
    P1 = models.FloatField(verbose_name="P1", blank=True, null=True)
    P2 = models.FloatField(verbose_name="P2", blank=True, null=True)
    P3 = models.FloatField(verbose_name="P3", blank=True, null=True)
    Q = models.FloatField(verbose_name="Q", blank=True, null=True)
    Q_Total = models.FloatField(verbose_name="Q_Total", blank=True, null=True)
    dt = models.FloatField(verbose_name="dt", blank=True, null=True)
    dV = models.FloatField(verbose_name="dV", blank=True, null=True)
    dM = models.FloatField(verbose_name="dM", blank=True, null=True)
    ta = models.FloatField(verbose_name="ta", blank=True, null=True)
    tcw = models.FloatField(verbose_name="tcw", blank=True, null=True)
    Pcw = models.FloatField(verbose_name="Pcw", blank=True, null=True)
    TGmax = models.FloatField(verbose_name="TGmax", blank=True, null=True)
    TGmin = models.FloatField(verbose_name="TGmin", blank=True, null=True)
    Tdt = models.FloatField(verbose_name="Tdt", blank=True, null=True)
    TFault = models.FloatField(verbose_name="TFault", blank=True, null=True)
    Toff = models.FloatField(verbose_name="Toff", blank=True, null=True)
    TOtherNS = models.FloatField(verbose_name="TOtherNS", blank=True, null=True)
    QntHIP = models.FloatField(verbose_name="QntHIP", blank=True, null=True)
    QntHIP_Total = models.FloatField(verbose_name="QntHIP_Total", blank=True, null=True)
    QntP = models.FloatField(verbose_name="QntP", blank=True, null=True)
    QntP_Total = models.FloatField(verbose_name="QntP_Total", blank=True, null=True)
    ns = models.BooleanField(verbose_name="ns", blank=True, null=True)
    empty = models.BooleanField(verbose_name="empty", blank=True, null=True)

    def __str__(self):
        return f"{self.idPoint} - {self.date} - ТС - часовое"

    class Meta:
        verbose_name = "Показание ТС - часовое"
        verbose_name_plural = "Показания ТС - часовые"


# Модель отображаемых параметров в таблице с показаниями
class CheckParams(models.Model):
    username = models.OneToOneField(User, on_delete=models.CASCADE, unique=True, verbose_name="id пользователя", blank=True, null=True)
    gvs_date = models.BooleanField(verbose_name="Дата", blank=True, null=True)
    gvs_dateOnEndOfArchive = models.BooleanField(verbose_name="dateOnEndOfArchive", blank=True, null=True)
    gvs_dateWithTimeBias = models.BooleanField(verbose_name="dateWithTimeBias", blank=True, null=True)
    gvs_Q1 = models.BooleanField(verbose_name="Q1", blank=True, null=True)
    gvs_Q1_Total = models.BooleanField(verbose_name="Q1 Итого", blank=True, null=True)
    gvs_Q2 = models.BooleanField(verbose_name="Q2", blank=True, null=True)
    gvs_Q2_Total = models.BooleanField(verbose_name="Q2 итого", blank=True, null=True)
    gvs_t1 = models.BooleanField(verbose_name="t1", blank=True, null=True)
    gvs_t2 = models.BooleanField(verbose_name="t2", blank=True, null=True)
    gvs_V1 = models.BooleanField(verbose_name="V1", blank=True, null=True)
    gvs_V1_Total = models.BooleanField(verbose_name="V1 Итого", blank=True, null=True)
    gvs_V2 = models.BooleanField(verbose_name="V2", blank=True, null=True)
    gvs_V2_Total = models.BooleanField(verbose_name="V2 Итого", blank=True, null=True)
    gvs_M1 = models.BooleanField(verbose_name="M1", blank=True, null=True)
    gvs_M1_Total = models.BooleanField(verbose_name="M1 Итого", blank=True, null=True)
    gvs_M2 = models.BooleanField(verbose_name="M2", blank=True, null=True)
    gvs_M2_Total = models.BooleanField(verbose_name="M2 Итого", blank=True, null=True)
    gvs_P1 = models.BooleanField(verbose_name="P1", blank=True, null=True)
    gvs_P2 = models.BooleanField(verbose_name="P2", blank=True, null=True)
    gvs_Q = models.BooleanField(verbose_name="Q", blank=True, null=True)
    gvs_Q_Total = models.BooleanField(verbose_name="Q Итого", blank=True, null=True)
    gvs_dt = models.BooleanField(verbose_name="Δt", blank=True, null=True)
    gvs_dV = models.BooleanField(verbose_name="ΔV", blank=True, null=True)
    gvs_dM = models.BooleanField(verbose_name="ΔM", blank=True, null=True)
    gvs_tcw = models.BooleanField(verbose_name="tхв", blank=True, null=True)
    gvs_Pcw = models.BooleanField(verbose_name="Pхв", blank=True, null=True)
    gvs_TGmax = models.BooleanField(verbose_name="TGmax", blank=True, null=True)
    gvs_TGmin = models.BooleanField(verbose_name="TGmin", blank=True, null=True)
    gvs_Tdt = models.BooleanField(verbose_name="TΔt", blank=True, null=True)
    gvs_TFault = models.BooleanField(verbose_name="TFault", blank=True, null=True)
    gvs_Toff = models.BooleanField(verbose_name="Toff", blank=True, null=True)
    gvs_TOtherNS = models.BooleanField(verbose_name="T прочие НС", blank=True, null=True)
    gvs_QntHIP = models.BooleanField(verbose_name="Тр", blank=True, null=True)
    gvs_QntHIP_Total = models.BooleanField(verbose_name="Тр Итого", blank=True, null=True)
    gvs_QntP = models.BooleanField(verbose_name="Тн", blank=True, null=True)
    gvs_QntP_Total = models.BooleanField(verbose_name="Тн Итого", blank=True, null=True)
    gvs_ns = models.BooleanField(verbose_name="НС", blank=True, null=True)
    gvs_empty = models.BooleanField(verbose_name="Пустое", blank=True, null=True)
    ts_date = models.BooleanField(verbose_name="Дата", blank=True, null=True)
    ts_dateOnEndOfArchive = models.BooleanField(verbose_name="dateOnEndOfArchive", blank=True, null=True)
    ts_dateWithTimeBias = models.BooleanField(verbose_name="dateWithTimeBias", blank=True, null=True)
    ts_Q1 = models.BooleanField(verbose_name="Q1", blank=True, null=True)
    ts_Q1_Total = models.BooleanField(verbose_name="Q1 Итого", blank=True, null=True)
    ts_Q2 = models.BooleanField(verbose_name="Q2", blank=True, null=True)
    ts_Q2_Total = models.BooleanField(verbose_name="Q2 Итого", blank=True, null=True)
    ts_t1 = models.BooleanField(verbose_name="t1", blank=True, null=True)
    ts_t2 = models.BooleanField(verbose_name="t2", blank=True, null=True)
    ts_t3 = models.BooleanField(verbose_name="t3", blank=True, null=True)
    ts_V1 = models.BooleanField(verbose_name="V1", blank=True, null=True)
    ts_V1_Total = models.BooleanField(verbose_name="V1 Итого", blank=True, null=True)
    ts_V2 = models.BooleanField(verbose_name="V2", blank=True, null=True)
    ts_V2_Total = models.BooleanField(verbose_name="V2 Итого", blank=True, null=True)
    ts_V3 = models.BooleanField(verbose_name="V3", blank=True, null=True)
    ts_V3_Total = models.BooleanField(verbose_name="V3 Итого", blank=True, null=True)
    ts_M1 = models.BooleanField(verbose_name="M1", blank=True, null=True)
    ts_M1_Total = models.BooleanField(verbose_name="M1 Итого", blank=True, null=True)
    ts_M2 = models.BooleanField(verbose_name="M2", blank=True, null=True)
    ts_M2_Total = models.BooleanField(verbose_name="M2 Итого", blank=True, null=True)
    ts_M3 = models.BooleanField(verbose_name="M3", blank=True, null=True)
    ts_M3_Total = models.BooleanField(verbose_name="M3 Итого", blank=True, null=True)
    ts_P1 = models.BooleanField(verbose_name="P1", blank=True, null=True)
    ts_P2 = models.BooleanField(verbose_name="P2", blank=True, null=True)
    ts_P3 = models.BooleanField(verbose_name="P3", blank=True, null=True)
    ts_Q = models.BooleanField(verbose_name="Q", blank=True, null=True)
    ts_Q_Total = models.BooleanField(verbose_name="Q Итого", blank=True, null=True)
    ts_dt = models.BooleanField(verbose_name="Δt", blank=True, null=True)
    ts_dV = models.BooleanField(verbose_name="ΔV", blank=True, null=True)
    ts_dM = models.BooleanField(verbose_name="ΔM", blank=True, null=True)
    ts_ta = models.BooleanField(verbose_name="ta", blank=True, null=True)
    ts_tcw = models.BooleanField(verbose_name="tхв", blank=True, null=True)
    ts_Pcw = models.BooleanField(verbose_name="Pхв", blank=True, null=True)
    ts_TGmax = models.BooleanField(verbose_name="TGmax", blank=True, null=True)
    ts_TGmin = models.BooleanField(verbose_name="TGmin", blank=True, null=True)
    ts_Tdt = models.BooleanField(verbose_name="TΔt", blank=True, null=True)
    ts_TFault = models.BooleanField(verbose_name="TFault", blank=True, null=True)
    ts_Toff = models.BooleanField(verbose_name="Toff", blank=True, null=True)
    ts_TOtherNS = models.BooleanField(verbose_name="T прочие НС", blank=True, null=True)
    ts_QntHIP = models.BooleanField(verbose_name="Тр", blank=True, null=True)
    ts_QntHIP_Total = models.BooleanField(verbose_name="Тр Итого", blank=True, null=True)
    ts_QntP = models.BooleanField(verbose_name="Тн", blank=True, null=True)
    ts_QntP_Total = models.BooleanField(verbose_name="Тн Итого", blank=True, null=True)
    ts_ns = models.BooleanField(verbose_name="НС", blank=True, null=True)
    ts_empty = models.BooleanField(verbose_name="Пустое", blank=True, null=True)

    def __str__(self):
        return f"{self.username} - поля"

    class Meta:
        verbose_name = "Активное поле"
        verbose_name_plural = "Активные поля"
