from django.urls import path, include
from . import views

urlpatterns = [
    path('export/list/', views.export_list, name='export_list'),
    path('export/gvs/', views.export_gvs, name='export_gvs'),
    path('export/ts/', views.export_ts, name='export_ts'),
    path('', views.DoubleListPUView.as_view(), name='double'),
    path('detail/<str:pk>', views.DoubleDetailView.as_view(), name='double_detail'),
    path('login', views.ConnectLogin.as_view(), name='login_page'),
    path('logout', views.ConnectLogout.as_view(), name='logout_page'),
    path('update_check/<int:pk>', views.UpdateCheckParams.as_view(), name='update_check'),
]
